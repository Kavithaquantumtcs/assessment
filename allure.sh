# scripts/install-allure.sh
#!/bin/bash
# Install Allure command-line tool
wget -O allure-2.13.9.tgz https://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline/2.13.9/allure-commandline-2.13.9.tgz
tar -xzvf allure-2.13.9.tgz
export PATH=$PWD/allure-2.13.9/bin:$PATH
